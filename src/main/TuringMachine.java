package main;

public class TuringMachine {
	
	private final int MAXITERATIONS = 84;
	
	private Node currentNode;
	private char[] tape;
	private int tapePos;

	public TuringMachine (Node start, char[] tape, int tapePos) {
		currentNode = start;
		this.tape = tape;
		this.tapePos = tapePos;		
	}
	
	public void run() {		
		int maxIterations = MAXITERATIONS;
		int i = 0;
		while (i < maxIterations) {
			
			System.out.print("Current node: " + currentNode.name + "  ");
									
			
			// Find transition corresponding to current tape symbol
			Transition currentTransition = null;
			for (Transition transition : currentNode.transitionList) {
				if (transition.symbolRead == tape[tapePos]) {
					currentTransition = transition;
					break;
				}
			}
			if (currentTransition == null) {
				System.out.println("Transition not found on this node. Ending.");
				break;
			}
			
			System.out.println("Transition found: "
							  + currentTransition.destination.name + "/"
							  + currentTransition.symbolWrite
							  + " "
							  + currentTransition.direction);
			
			printTape();
			
			// Change tape symbol
			tape[tapePos] = currentTransition.symbolWrite;
			
			// Update tape position
			switch (currentTransition.direction) {
				case "right": if (tapePos >= tape.length) tapePos = -1;
							   else tapePos++;
							   break;
				case "left":  if (tapePos < 0) tapePos = -1;
				  			   else tapePos--;
				   			   break;
				default:      System.out.println("Destination name error.");
							   break;
			}
			if (tapePos == -1) {
				System.out.println("Tape position " + tapePos + " out of bounds. Ending.");
				break;
			}
			
			// Update node
			currentNode = currentTransition.destination;
			
			i++;
		}
		
	}

	private void printTape() {
		for (int i = 0; i < tape.length; i++) {
			if (i != tapePos) 
				System.out.print(" " + tape[i] + " ");
			else 
				System.out.print("<" + tape[i] + ">");
		
		}
		System.out.println();
	}
	
	public static void main (String[] args) {
		Node a = new Node("a");
		Node b = new Node("b");
		Node c = new Node("c");
		Node d = new Node("d");
		Node e = new Node("e");
		Node f = new Node("f");
		Node g = new Node("g");
		Node h = new Node("h");
		Node i = new Node("i");
				
		a.transitionList.add(new Transition(b, "left",  'B', 'B'));
		
		b.transitionList.add(new Transition(b, "left",  '0', '0'));
		b.transitionList.add(new Transition(c, "left",  '1', '1'));
		
		c.transitionList.add(new Transition(c, "left",  '0', '0'));
		c.transitionList.add(new Transition(d, "right", '1', 'B'));
		
		d.transitionList.add(new Transition(e, "left",  'B', '1'));
		d.transitionList.add(new Transition(d, "right", '0', '0'));
		d.transitionList.add(new Transition(d, "right", '1', '1'));
		
		e.transitionList.add(new Transition(f, "right", '0', 'B'));	

		f.transitionList.add(new Transition(g, "left",  'B', '0'));
		f.transitionList.add(new Transition(f, "right", '0', '0'));
		f.transitionList.add(new Transition(f, "right", '1', '1'));
		
		g.transitionList.add(new Transition(g, "left",  '0', '0'));
		g.transitionList.add(new Transition(g, "left",  '1', '1'));
		g.transitionList.add(new Transition(h, "left",  'B', '0'));
		
		h.transitionList.add(new Transition(f, "right", '0', 'B'));
		h.transitionList.add(new Transition(h, "left",  '1', '1'));
		h.transitionList.add(new Transition(i, "right", 'B', '1'));
		
		i.transitionList.add(new Transition(i, "right", '0', '0'));
		i.transitionList.add(new Transition(i, "right", '1', '1'));
		i.transitionList.add(new Transition(b, "left", 'B', 'B'));
		
		String tape = "1010BBBBBBBBBB";
				
		TuringMachine turing = new TuringMachine(a, tape.toCharArray(), 4);
		turing.run();
	}
	
}
