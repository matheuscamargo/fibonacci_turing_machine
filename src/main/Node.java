package main;

import java.util.LinkedList;
import java.util.List;

public class Node {
	public final String name;
	public final List<Transition> transitionList = new LinkedList<>();	
	
	public Node(String name) {
		this.name = name;
	}	
}
