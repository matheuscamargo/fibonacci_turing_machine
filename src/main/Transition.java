package main;

public class Transition {
	public final Node destination;
	public final String direction;
	public final char symbolRead;
	public final char symbolWrite;
	
	public Transition(Node destination, String direction, char symbolRead, char symbolWrite) {
		this.destination = destination;
		this.direction = direction;
		this.symbolRead = symbolRead;
		this.symbolWrite = symbolWrite;
	}	
}
